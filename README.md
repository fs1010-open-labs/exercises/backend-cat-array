## FS1010 Open Lab 1
This is Part 1 of a 2-part lab, we will learn to pull information from our array to our front-end React app.

## Scenario
Jen has hired you to create a website for her cat cafe! It will be a landing page that shows all the cats available for adoption at her cafe.
You have been given the following materials in the Google Drive link here:
https://docs.google.com/spreadsheets/d/1JR0eDHVfm4Z50dxGVJsHPFzoECaOKbHK2BfExPWC35A/edit?usp=sharing

1. Cat names
2. Images of the cats to be adopted

Figma design here: https://www.figma.com/file/enllL85WtXAG2sz8NduBMd/cat-cafe-connecting-msql?node-id=0%3A1



## Back-End Instructions

## Part 1: Set up back-end files
*Note: this is FS1020 review, so please use Affaf's labs if you get stuck!

1. Create a new Gitlab repo and name it `backend-array-cat-cafe`

2. Clone your repo

3. `npm install express esm` and `nodemon`

4. `npm init` to create a new package.json file

5. create an `index.js` - you will add your routes here

6. create a `dev` script so you can run nodemon. 
    - See docs here: https://www.npmjs.com/package/nodemon
    - Hint: You can find this in Affaf's FS1020 labs

7. don't forget to `.gitignore node_modules`

8. create a test "/" GET route to test if your API is working on Postman - have it listen to `port 3001` (NOT 3000)
    - Why port 3001?  React App is listening to port 3000 so use a different port. Your React front end will be taking information from :3001 (see diagram): https://miro.com/app/board/o9J_lZizQCU=/


## Part 2: Create and set up array database
1. Create a new file `cats.js` and put this information in an array:
https://docs.google.com/spreadsheets/d/1JR0eDHVfm4Z50dxGVJsHPFzoECaOKbHK2BfExPWC35A/edit?usp=sharing

2. In index.js (if you want to go above and beyond, create a router.js or separate your routes) - create a GET("/api/cats") route and pull all cats from the `cats.js` array


## HOMEWORK for next lab: Finish GET/POST/PUT/DELETE routes
- GET - shows the list of cats
- POST - adds one cat to the db and then shows the updated list of cats
- PUT - edits one cat profile and then shows the new cat profile
- DELETE - deletes a cat and then shows the updated list of cats







